var dgram = require('dgram');

var server = dgram.createSocket("udp4")

server.bind(8089,"127.27.1.1")
server.on("message", function (message) {

    var output = "Udp server receive message : " + message + "\n";
    process.stdout.write(output);
});

server.on('listening', function () {
    var address = server.address(); 
    console.log('UDP Server started and listening on ' + address.address + ":" + address.port);
});